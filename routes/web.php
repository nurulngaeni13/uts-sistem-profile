<?php

use App\Http\Controllers\AdminController73;
use App\Http\Controllers\UsersController73;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return redirect('/register73');
});

Route::group(['middleware' => ['isNotLogged']], function () {
    Route::view('/register73', 'register');
    Route::view('/login73', 'login');
    Route::post('/register73', [UsersController73::class, 'registerHandler73']);
    Route::post('/login73', [UsersController73::class, 'loginHandler73']);
});

Route::group(['middleware' => ['isUser']], function () {
    Route::get('/profile73', [UsersController73::class, 'profilePage73']);
    Route::get('/changePassword73', [UsersController73::class, 'editPasswordPage73']);
    Route::post('/updatePassword73', [UsersController73::class, 'updatePassword73']);
    Route::post('/updateProfil73', [UsersController73::class, 'updateProfil73']);
    Route::post('/uploadPhotoProfil73', [UsersController73::class, 'uploadPhotoProfil73']);
    Route::post('/uploadPhotoKTP73', [UsersController73::class, 'uploadPhotoKTP73']);
});

Route::group(['middleware' => ['isAdmin']], function () {
    Route::get('/dashboard73', [AdminController73::class, 'dashboardPage73']);
    Route::get('/detail73/{id}', [AdminController73::class, 'detailPage73']);
    Route::get('/update73/user/{id}/status', [AdminController73::class, 'updateUserStatus73']);
    Route::post('/update73/user/{id}/agama', [AdminController73::class, 'updateUserAgama73']);

    Route::get("/agama73", [AdminController73::class, "agamaPage73"]);
    Route::post("/agama73", [AdminController73::class, "createAgama73"]);
    Route::get("/agama73/{id}/edit", [AdminController73::class, 'editAgamaPage73']);
    Route::post("/agama73/{id}/update", [AdminController73::class, 'updateAgama73']);
    Route::get("/agama73/{id}/delete", [AdminController73::class, 'deleteAgama73']);
});

Route::get('/logout73', [UsersController73::class, 'logoutHandler73'])->middleware('isLogged');
